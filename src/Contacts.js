import React, {useState} from 'react';


const Contacts = ({sorted, contacts}) => {
    const [check, setCheck] = useState(false);
    const [allData, setAllData] = useState(sorted);
    const [filteredData, setFilteredData] = useState(allData)

    console.log(allData);
    const handleSearch = (e) => {
        let value = e.target.value.toLowerCase();
        let result = [];
        result = allData.filter((data) => {
            return (data.first_name || data.last_name).search(value) !== -1;
        });
        setFilteredData(result)
    }

    const table = filteredData.map((el) => (
        <tr key={el.id} onClick={() => console.log(el.id)}>
            <td><img alt="avatar" src={el.avatar}/></td>
            <td>{el.first_name}</td>
            <td>{el.last_name}</td>
            <td>{el.email}</td>
            <td>{<input className="form-check-input" type="checkbox" value="" id={el.id} name={el.id}
                        onChange={() => setCheck(!check)}/>}</td>
        </tr>
    ));
    return (
        <>
            <input className="form-control" type="text" placeholder="Search"
                   onChange={(e) => handleSearch(e)}/>
            <table className="table table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">Avatar</th>
                    <th scope="col">First</th>
                    <th scope="col">Last</th>
                    <th scope="col">Email</th>
                    <th scope="col">Selected</th>
                </tr>
                </thead>
                <tbody>
                {table}
                </tbody>
            </table>
        </>

    )
};

export default Contacts;