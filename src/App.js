import React, {useEffect, useState} from 'react';
import Contacts from "./Contacts";
import axios from "axios";

const App = () => {

    const [contacts, setContacts] = useState([])

    let sorted = [...contacts]
    sorted.sort((a, b) => {
        if (a.last_name < b.last_name) {
            return -1
        }

        if (a.last_name > b.last_name) {
            return 1
        }
        return 0
    })

    useEffect(() => {
        axios.get("https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json")
            .then(response => {
                setContacts(response.data)
            })
            .catch(error => {
                console.error(error)
            })
    }, [])

    return (
        <>
            <h1>Contacts</h1>
            <Contacts sorted={sorted} contacts={contacts}/>
        </>
    )
};

export default App;